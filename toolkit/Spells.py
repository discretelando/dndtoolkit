#Written By Ricardo
#CIS 422
#06/04/2019
import sqlite3 
from tkinter import *
from tkinter import ttk
import tkinter as tk
from tkinter import messagebox
import types
import csv
"""
This script will interface with the database to keep track of all spells a character has come across. 
Displaying the spells basic information such as name, range, damage, duration, etc. Serves more as a reference sheet.
"""
class Spells(Frame):
    def __init__(self, name = None, master=None):
            '''
            Constructor
            '''
            Frame.__init__(self, master)
            #name is passed in from Campaign
            self.name = name
            self.tree=ttk.Treeview(master, show = "headings")
            self.conn = sqlite3.connect('toolkit.sqlite')
            cur = self.conn.cursor()
            cur.execute("SELECT * FROM spells WHERE name=?", (self.name,)) #db query to check for matching tables. Which we require to enter this view
            rows = cur.fetchall()
            cur.close()
            self.pathtofile = rows[0][2] #3rd column value
            self.count = rows[0][1] #2nd column value
            with open(self.pathtofile,"r") as file:
            	reader = csv.reader(file)
            	self.row = next(reader)
            if self.count == 3:
            	self.tree["columns"]=("one","two","three") #takes the count/size of the requested spellsheet and creates the widgets based on that requirement
            	self.tree.heading("#1", text = self.row[0]) #row[x] sets the name/header of the column
            	self.tree.heading("#2", text = self.row[1])
            	self.tree.heading("#3", text = self.row[2])
            elif self.count == 4:
            	self.tree["columns"]=("one","two","three", "four")
            	self.tree.heading("#1", text = self.row[0])
            	self.tree.heading("#2", text = self.row[1])
            	self.tree.heading("#3", text = self.row[2])
            	self.tree.heading("#4", text = self.row[3])
            elif self.count == 5:
            	self.tree["columns"]=("one","two","three", "four", "five")
            	self.tree.heading("#1", text = self.row[0])
            	self.tree.heading("#2", text = self.row[1])
            	self.tree.heading("#3", text = self.row[2])
            	self.tree.heading("#4", text = self.row[3])
            	self.tree.heading("#5", text = self.row[4])
            elif self.count == 6:
            	self.tree["columns"]=("one","two","three", "four", "five", "six")
            	self.tree.heading("#1", text = self.row[0])
            	self.tree.heading("#2", text = self.row[1])
            	self.tree.heading("#3", text = self.row[2])
            	self.tree.heading("#4", text = self.row[3])
            	self.tree.heading("#5", text = self.row[4])
            	self.tree.heading("#6", text = self.row[5])
            elif self.count == 7:
            	self.tree["columns"]=("one","two","three", "four", "five", "six", "seven")
            	self.tree.heading("#1", text = self.row[0])
            	self.tree.heading("#2", text = self.row[1])
            	self.tree.heading("#3", text = self.row[2])
            	self.tree.heading("#4", text = self.row[3])
            	self.tree.heading("#5", text = self.row[4])
            	self.tree.heading("#6", text = self.row[5])
            	self.tree.heading("#7", text = self.row[6])
            elif self.count == 8:
            	self.tree["columns"]=("one","two","three", "four", "five", "six", "seven", "eight")
            	self.tree.heading("#1", text = self.row[0])
            	self.tree.heading("#2", text = self.row[1])
            	self.tree.heading("#3", text = self.row[2])
            	self.tree.heading("#4", text = self.row[3])
            	self.tree.heading("#5", text = self.row[4])
            	self.tree.heading("#6", text = self.row[5])
            	self.tree.heading("#7", text = self.row[6])
            	self.tree.heading("#8", text = self.row[7])
            elif self.count == 9:
            	self.tree["columns"]=("one","two","three", "four", "five", "six", "seven", "eight", "nine")
            	self.tree.heading("#1", text = self.row[0])
            	self.tree.heading("#2", text = self.row[1])
            	self.tree.heading("#3", text = self.row[2])
            	self.tree.heading("#4", text = self.row[3])
            	self.tree.heading("#5", text = self.row[4])
            	self.tree.heading("#6", text = self.row[5])
            	self.tree.heading("#7", text = self.row[6])
            	self.tree.heading("#8", text = self.row[7])
            	self.tree.heading("#9", text = self.row[8])
            elif self.count == 10:
            	self.tree["columns"]=("one","two","three", "four", "five", "six", "seven", "eight", "nine", "ten")
            	self.tree.heading("#1", text = self.row[0])
            	self.tree.heading("#2", text = self.row[1])
            	self.tree.heading("#3", text = self.row[2])
            	self.tree.heading("#4", text = self.row[3])
            	self.tree.heading("#5", text = self.row[4])
            	self.tree.heading("#6", text = self.row[5])
            	self.tree.heading("#7", text = self.row[6])
            	self.tree.heading("#8", text = self.row[7])
            	self.tree.heading("#9", text = self.row[8])
            	self.tree.heading("#10", text = self.row[9])
            self.pack()
            self.createWidgets()
            self.tree.pack(side="top", fill="both", expand=True)
            master.bind('q', self.BACK['command'])
            master.protocol("WM_DELETE_WINDOW", self.quit)
            self.View()

    def View(self): #populates the tree with information from the file
    	self.fixup()
    	self.cleanup()
    	file = open(self.pathtofile, "r")
    	with file:
    		reader = csv.reader(file)
    		next(reader)
    		for row in reader:
    			self.tree.insert("", tk.END, values=row)
    
    def createWidgets(self): #creates widgets to be added to the view
    	self.addSpellB = Button(self, text = "Add Spell", command = lambda: self.addSpell())
    	self.deleteSpellB = Button(self, text = "Delete Spell", command = lambda: self.deleteSpell())
    	self.BACK = Button(self, text = "BACK <q>", command = self.quit)
    	self.BACK["fg"]   = "red"

    	self.addSpellB.pack({"side" : "top"})
    	self.deleteSpellB.pack({"side" : "top"})
    	self.BACK.pack({"anchor":"e"})

    def addSpell(self):
    	names = []
    	linect = 0
    	file = open(self.pathtofile)
    	for line in file:
    		linect+=1
    	for var in range(self.count):
    		if var == 0:
    			newspell_input = simpledialog.askstring("New Value", self.row[var])
    			names.append(str(linect)+". "+newspell_input) #adds the line count and appends to the front of the new user input
    		else:
    			newspell_input = simpledialog.askstring("New Value", self.row[var])
    			names.append(newspell_input) #appends the old row to the file
    	file = open(self.pathtofile, "a")
    	with file:
    		writer = csv.writer(file)
    		writer.writerow(names)
    	self.View()#calls View to populate the data newly added to the file

    def deleteSpell(self): #deletes a spell given the line number
    	deleteline = simpledialog.askstring("Delete", "Enter Line Number")
    	if(self.intcheck(deleteline)):
    		choice = int(float(deleteline))
    		with open(self.pathtofile, "r") as infile: #reads the file and stores input into a list before file gets overwritten
    			lines = infile.readlines()
    		with open(self.pathtofile, "w") as outfile:
    			for pos, line in enumerate(lines):
    				if pos != choice and pos != 0:
    					line[2:]
    					outfile.write(line)
    				elif pos != choice:
    					outfile.write(line)
    		self.View()

    	else:
    		messagebox.showwarning("OOPS!", "That there is not an INTEGER!")

    def intcheck(self, check): #see Campaign.py for more information
    	try:
    		test = int(float(check))
    		if(isinstance(test, int)):
    			return True
    	except:
    		return False

    def fixup(self): #see Stats.py for more information
    	with open(self.pathtofile, "r") as infile:
    		lines = infile.readlines()
    	with open(self.pathtofile, "w") as outfile:
    		for pos, line in enumerate(lines):
    			if pos != 0:
    				outfile.write(str(pos)+". "+line[3:])
    			else:
    				outfile.write(line)
    
    def cleanup(self): #clears tree in order to receive new data
    	for row in self.tree.get_children():
    		self.tree.delete(row)
