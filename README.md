==MYTHICAL DICE SYSTEM 1.0 README===

Updated: 6/5/19



--PROGRAM AUTHORS--

Jonathan Fujii - Mythical Dice Functionality Developer

Jeremy King - Mythical Dice Functionality Developer

Jake Gianola - Mythical Dice UI Developer

Cameron McKeown - Documentation and slight Toolkit UI

Rico Williams - Toolkit Developer 

*See comments in code for specific distribution by files*



--BRIEF DESCRIPTION--

This program allows for a person to make

a customized die that emulates a physical die.

This die is saved, updated, and can be subsequently

rolled as per a normal die. The toolkit aspect allows

users to save data regarding their game, effectively

and without effort.



--CREATION DOCUMENTATION--

Mythical Dice and Toolkit was made over the month of May 2019.

It was created for the second project assignment of 
CIS422:
Software Methodologies.




--SETUP INSTRUCTIONS--

For application setup instructions, please see:

Installation_Instructions.txt




--USER INSTRUCTIONS--

For user documentation for application, please see:

User_Documentation.pdf




--DEVELOPER/PROGRAMMER INSTRUCTIONS--

For developer documentation, please see:

Programmer_Documentation.pdf




--SOFTWARE DEPENDENCIES--

Mythical Dice and Toolkit were designed for Python 3.7.1 and intended

to be used on Macintosh OSX 10.13 or 10.14

or Windows 10 OS


Dice Roller aspect requires the external Python library: Pygame 



--SUBDIRECTORY INFORMATION--

src: Has all the dice roller python files and assets

toolkit: Has all toolkit python files and assets