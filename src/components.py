# CIS 422 Project 2 – Pygame GUI Components
# Jeremy King
# 5/21/2019

# This program requires the pygame module.
# To install it run "pip3 install pygame" or "pip install pygame" from the command line
# ref: https://www.pygame.org/docs/

import os
import pygame
import random as r
from math import *

try: # assume one directory up
	from src.diceobject import *
except ImportError: # assume same directory
	from diceobject import *

ROOT = os.path.dirname(os.path.abspath(__file__)) # project_dir/dice/

COLOR_TEXT = pygame.Color(240, 240, 240)
COLOR_WHITE = pygame.Color(250, 250, 250)
COLOR_BLACK = pygame.Color(10, 10, 10)
COLOR_BACKGROUND = pygame.Color(10, 10, 10)
COLOR_PRIMARY = pygame.Color(45, 180, 135)
COLOR_SILENT = pygame.Color(45, 135, 180)
COLOR_DELETE = pygame.Color(180, 55, 45)

# Component
class Component:
	def hover(self, pos:tuple) -> bool: # try hovering
		return False
	def click(self, pos:tuple) -> bool: # try clicking
		return False
	def draw(self, screen, forcedraw=False): # draw component
		pass
	def _check(self, pos:tuple) -> bool: # check if pos collides with component
		return self.rect.collidepoint(pos)

# Line
class Line(Component):
	def __init__(self, pos1:tuple, pos2:tuple):
		self.pos1 = pos1
		self.pos2 = pos2
		self.color = COLOR_TEXT

	def draw(self, screen, forcedraw=False):
		pygame.draw.line(screen, self.color, self.pos1, self.pos2, 1)


# Button
class Button(Component):
	def __init__(self, pos:tuple, w:int, h:int, font, name:str, color=COLOR_PRIMARY):
		self.rect = pygame.Rect(pos, (w, h))
		self.font = font
		self.name = name

		self.color = color

		# white fade
		self.fade = pygame.Surface((w, h))
		self.fade.set_alpha(50)
		self.fade.fill(COLOR_WHITE)

		self.hovering = False
		self.redraw = True

	def hover(self, pos:tuple) -> bool:
		if (self._check(pos) != self.hovering):
			self.hovering = not self.hovering
			self.redraw = True
			return True
		return False
	
	def click(self, pos:tuple) -> bool:
		if self._check(pos):
			# print("Button \""+self.name+"\" clicked.")
			self.hovering = False
			self.redraw = True
			return True
		return False

	def draw(self, screen, forcedraw=False):
		if (not forcedraw and not self.redraw): # redraw not necessary
			return

		pygame.draw.rect(screen, self.color, self.rect) # fill
		text = self.font.render(self.name, True, COLOR_BLACK)
		x = self.rect.x + (self.rect.w/2 - text.get_width()/2)
		y = self.rect.y + (self.rect.h/2 - text.get_height()/2)
		screen.blit(text, (x, y))

		if self.hovering:
			screen.blit(self.fade, (self.rect.x, self.rect.y)) # fade
		self.redraw = False

	def set_text(self, text):
		self.name = text
		self.redraw = True

# CheckBox
class CheckBox(Component):
	def __init__(self, pos:tuple, w:int, h:int, font, name:str):
		self.rect = pygame.Rect(pos, (w, h))
		self.font = font
		self.name = name

		self.color = COLOR_PRIMARY
		self.color_hover = pygame.Color(20, 80, 65)

		pad = 2
		self.border = 2
		self.check_rect = pygame.Rect(self.rect.x+pad, self.rect.y+pad, self.rect.h-2*pad, self.rect.h-2*pad)
		self.cent_rect = pygame.Rect(self.rect.x+2*pad+self.border, self.rect.y+2*pad+self.border, self.rect.h-4*pad-2*self.border, self.rect.h-4*pad-2*self.border)
		self.text_pos = (self.check_rect.x+self.check_rect.w+pad*4, (self.rect.y+self.check_rect.h/2+pad*2)-self.font.get_height()/2)

		self.checked = False
		self.hovering = False
		self.redraw = True

	def hover(self, pos:tuple) -> bool:
		if (self._check(pos) != self.hovering):
			self.hovering = not self.hovering
			self.redraw = True
			return True
		return False
	
	def click(self, pos:tuple) -> bool:
		if self._check(pos):
			# print("CheckBox \""+self.name+"\" clicked.")
			self.hovering = False
			self.redraw = True
			self.checked = not self.checked
			return True
		return False

	def draw(self, screen, forcedraw=False):
		if (not forcedraw and not self.redraw): # redraw not necessary
			return

		pygame.draw.rect(screen, COLOR_BACKGROUND, self.rect) # clear
		pygame.draw.rect(screen, COLOR_TEXT, self.check_rect, self.border) # box
		if self.checked:
			pygame.draw.rect(screen, self.color, self.cent_rect) # fill
		elif self.hovering:
			pygame.draw.rect(screen, self.color_hover, self.cent_rect) # fill
		
		text = self.font.render(self.name, True, COLOR_TEXT)
		screen.blit(text, self.text_pos)
		self.redraw = False

	def uncheck(self):
		self.checked = False
		self.redraw = True

# RollHistory
class RollHistory(Component):
	def __init__(self, pos:tuple, w:int, h:int, font):
		self.rect = pygame.Rect(pos, (w, h))
		self.font = font
		self.dice = None # set using load(dice)

		self.hovering = False
		self.redraw = True

	def draw(self, screen, forcedraw=False):
		if (not forcedraw and not self.redraw): # redraw not necessary
			return
		
		pygame.draw.rect(screen, COLOR_BACKGROUND, self.rect) # clear
		if self.dice == None: # no dice to draw
			return

		text1 = self.font.render("Avg: "+str(round(self.dice.average, 2)), True, COLOR_TEXT)
		text2 = self.font.render("Rolls:", True, COLOR_TEXT)
		h = self.font.get_height() + 5
		offset = 5
		screen.blit(text1, (self.rect.x+5, self.rect.y+offset))
		offset += h
		screen.blit(text2, (self.rect.x+5, self.rect.y+offset))
		offset += h
		rolls = self.dice.log.copy()
		rolls.reverse()
		for i in range(len(rolls)):
			a = 240-20*i
			text = self.font.render(str(rolls[i]), True, COLOR_TEXT)
			temp = pygame.Surface((text.get_width(), text.get_height()))
			temp.fill(COLOR_BACKGROUND)
			temp.blit(text, (0, 0))
			temp.set_alpha(a)
			screen.blit(temp, (self.rect.x+self.rect.w/2-5, self.rect.y+offset))
			offset += h
			if offset > self.rect.h:
				break
		self.redraw = False

	def load(self, diceObject):
		self.dice = diceObject
		self.redraw = True

# DiceDisplay
class DiceDisplay(Component):
	def __init__(self, pos:tuple, w:int, h:int, font, r:int):
		self.rect = pygame.Rect(pos, (w, h))
		self.font = font
		self.r = r # radius

		self.origin = (pos[0]+w/2, pos[1]+h-r-20)
		self.v0 = -10

		self.dice1 = None # set in load(dice)
		self.diceRoll1 = None # last rolled number of dice1
		self.dice2 = None # used for adv/dis
		self.diceRoll2 = None # last rolled number of dice2
		self.diceImage = None # set in load(dice)
		self.weightIcon = None
		self._load_weight_icon()
		# self.diceGraphic = None
		
		self.mode = 0 # 0=normal, 1=advantage, 2=disadvantage
		self.flip = False
		self.animating = False
		self.deleting = False
		self.showRoll = False
		self.t = 0 # animation time
		self.rotation = 0.0
		self.finished = False # flag checked to update RollHistory
		self.redraw = True

	def load(self, diceObject):
		self.flip = diceObject.dtype < 3
		self.dice1 = diceObject
		self.dice2 = diceObject
		self.diceImage = DiceImage(diceObject.image)
		self._reset_animation()
		self.redraw = True
		self.showRoll = False

	def draw(self, screen, forcedraw=False):
		if not self.redraw and not forcedraw:
			return

		pygame.draw.rect(screen, COLOR_BACKGROUND, self.rect) # clear
		if self.dice1 == None and not self.deleting:
			print("returning")
			return

		b = 70
		y = self.origin[1]
		if self.animating:
			self.rotation += 20
			self.t += 1
			y = 0.2*self.t*self.t+self.v0*self.t+self.origin[1]
			if y > self.origin[1]:
				self._reset_animation()
				self.finished = True
				self.showRoll = True
				y = self.origin[1]
		elif self.deleting:
			self.t += 2
			if 255 - self.t < 0:
				self.finished = True
				pygame.draw.rect(screen, COLOR_BACKGROUND, self.rect) # clear
				self.redraw = False
			else:
				self.diceImage.draw(screen, self.origin, None, opaque=True, opacity=255-self.t)
				self.redraw = True
			return


		if self.mode == 0:
			self.diceImage.draw(screen, (self.origin[0], y), self.rotation if self.animating else None, flip=self.flip)
		elif self.mode == 1:
			self.diceImage.draw(screen, (self.origin[0]-b, y), self.rotation if self.animating else None, opaque=(int(self.diceRoll1)<int(self.diceRoll2) and not self.animating), flip=self.flip)
			self.diceImage.draw(screen, (self.origin[0]+b, y), self.rotation if self.animating else None, opaque=(int(self.diceRoll2)<int(self.diceRoll1) and not self.animating), flip=self.flip)
		else:
			self.diceImage.draw(screen, (self.origin[0]-b, y), self.rotation if self.animating else None, opaque=(int(self.diceRoll1)>int(self.diceRoll2) and not self.animating), flip=self.flip)
			self.diceImage.draw(screen, (self.origin[0]+b, y), self.rotation if self.animating else None, opaque=(int(self.diceRoll2)>int(self.diceRoll1) and not self.animating), flip=self.flip)

		if self.showRoll:
			try:
				textRoll1 = self.font.render(str(self.diceRoll1), True, pygame.Color(self.dice1.fontcolorhtml))
			except:
				textRoll1 = self.font.render(str(self.diceRoll1), True, COLOR_BLACK)
			if self.mode == 0:
				screen.blit(textRoll1, (self.origin[0]-textRoll1.get_width()/2, self.origin[1]-textRoll1.get_height()/2))
			elif self.mode == 1 or self.mode == 2:
				try:
					textRoll2 = self.font.render(str(self.diceRoll2), True, pygame.Color(self.dice2.fontcolorhtml))
				except:
					textRoll2 = self.font.render(str(self.diceRoll2), True, COLOR_BLACK)
				screen.blit(textRoll1, (self.origin[0]-b-textRoll1.get_width()/2, self.origin[1]-textRoll1.get_height()/2))
				screen.blit(textRoll2, (self.origin[0]+b-textRoll2.get_width()/2, self.origin[1]-textRoll2.get_height()/2))
		
		if self.dice1 != None:
			text = self.font.render(self.dice1.name, True, COLOR_TEXT) # dice name
			screen.blit(text, (self.rect.x+5, self.rect.y+5))
			if self.dice1.weighted:
				screen.blit(self.weightIcon, (self.rect.x+self.rect.w-(5+self.weightIcon.get_width()), self.rect.y+5))
		self.redraw = self.animating
	
	def roll(self, mode:int=0, silent=False): # 0=normal, 1=advantage, 2=disadvantage
		self.mode = mode
		self.showRoll = False
		self.animating = True
		self.redraw = True
		if silent:
			self.diceRoll1 = self.dice1.rolldiesilent()
			if mode == 1 or mode == 2:
				self.diceRoll2 = self.dice2.rolldiesilent()
		else:
			self.diceRoll1 = self.dice1.rolldie()
			if mode == 1 or mode == 2:
				self.diceRoll2 = self.dice2.rolldie()

	def delete(self):
		self._reset_animation()
		self.dice1 = None
		self.redraw = True
		self.deleting = True

	def cancel_delete(self):
		self._reset_animation()
		self.dice1 = self.dice2
		self.redraw = True
		self.deleting = False

	def _reset_animation(self):
		self.animating = False
		self.rotation = 0
		self.t = 0

	def _load_weight_icon(self):
		# self.weightIcon
		file = os.path.join(ROOT, "diceassets", "weightedicon.png")
		try:
			self.weightIcon = pygame.transform.scale(pygame.image.load(file).convert_alpha(), (30, 30)) # load icon from file
		except:
			print("failed to load image \"weightedicon.png\"")
			self.weightIcon = self.font.render("W", True, COLOR_TEXT)

# DiceImage
class DiceImage():
	def __init__(self, staticImage, rollingImage=None):
		try:
			self.staticImage = pygame.image.load(os.path.join(ROOT, staticImage)).convert_alpha() # load image from file
		except:
			print("failed to load image \""+staticImage+"\"")
			self.staticImage = pygame.Surface((50, 50))
			self.staticImage.fill(COLOR_WHITE)

		if rollingImage == None:
			self.rollingImage = self.staticImage.copy()
			return

		try:
			self.rollingImage = pygame.image.load(os.path.join(ROOT, rollingImage)).convert_alpha() # load image from file
		except:
			print("failed to load image \""+rollingImage+"\"")
			self.rollingImage = self.staticImage.copy()

	def draw(self, screen, pos:tuple, rotation:float=None, opaque:bool=False, opacity:int=100, flip:bool=False):
		if flip:
			img = self.staticImage.copy() if rotation == None else pygame.transform.scale(self.rollingImage, (self.rollingImage.get_width(), int(abs(self.rollingImage.get_height()*cos(radians(rotation))))))
		else:
			img = self.staticImage.copy() if rotation == None else pygame.transform.rotate(self.rollingImage, rotation)
		if opaque:
			# ref: https://stackoverflow.com/questions/12879225/pygame-applying-transparency-to-an-image-with-alpha
			img.fill((255, 255, 255, opacity), None, pygame.BLEND_RGBA_MULT)
		screen.blit(img, (pos[0]-img.get_width()/2, pos[1]-img.get_height()/2))