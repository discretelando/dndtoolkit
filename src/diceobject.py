#Project 2 Mythical Dice 6/4/2019
#Component Authors: Jonathan Fujii, Jeremy King, Jake Gianola
# coding: utf-8

# In[18]:

import os
import random
class dice_obj:
    name = "MyDice"
    color = "RED"
    fontcolor = "BLACK"
    fontcolorhtml = "#000000"
    #Description of Dice
    notes = "This is my dice"
    #Type of die (ex. d6, d20, etc)
    dtype = 0
    result = 0
    log = []
    average = 0
    image = "diceassets/d20blue.png"
    #Boolean of if the dice is weighted or not
    weighted = False
    #Percentage of time the die will roll the favorednum
    weight = 0
    #The number more likely to be the result due to being weighted
    favorednum = 0

    def __init__(self, name, dtype, color, fontcolor, fontcolorhtml, weighted, favorednum, weight, log, lifetimerolls, average, image, notes):
        self.name = name
        self.dtype = int(dtype) #THIS IS VERY IMPORTANT THIS IS AN INT
        self.color = color
        self.fontcolor = fontcolor
        self.fontcolorhtml = fontcolorhtml
        self.weighted = (weighted == "True")
        self.favorednum = favorednum
        self.weight = float(weight)
        self.log = log.strip('[]').split(',')
        self.lifetimerolls = int(lifetimerolls)
        self.average = float(average)
        self.image = image
        self.notes = notes
    
    def createfromline(self, lineoftext):
        pass
    
    def setnotes(self, newnote):
        self.notes = newnote
        
    def getnotes(self):
        return self.notes
    
    def setname(self, newname):
        self.name = newname
        
    def getname(self):
        return self.name
    
    def updateavg(self,loglist):
        self.average = (self.average*(self.lifetimerolls-1)+float(loglist[-1]))/self.lifetimerolls
        
    def rolldie(self):
        self.lifetimerolls += 1
        self.result = random.randint(1,self.dtype)
        if(self.weight != 0):
            if (random.uniform(0,100) <= self.weight):
                self.result = self.favorednum
        #Code to put stuff into log:
        self.log.append(self.result)
        if len(self.log) > 10:
            self.log.remove(self.log[0])
        self.updateavg(self.log)
        self.updatefile()
        return self.result

    def rolldiesilent(self):
        self.result = random.randint(1,self.dtype)
        if(self.weight != 0):
            if (random.uniform(0,100) <= self.weight):
                self.result = self.favorednum
        return self.result
    
    
    def delete(self):
        rootpath = os.path.dirname(os.path.abspath(__file__)) #.../src/
        path = os.path.join(rootpath, "diceobjects", self.name+".txt")
        os.remove(path)
        
    
    def updatefile(self):
        rootpath = os.path.dirname(os.path.abspath(__file__)) #.../src/

        try:
            file = open(os.path.join(rootpath, "diceobjects", self.name+".txt"), "w+")
        except Exception as e:
            print("Error:", e)

        sep = ";"
        out = ""
        out += self.name+sep
        out += str(self.dtype)+sep
        out += self.color+sep
        out += self.fontcolor+sep
        out += self.fontcolorhtml+sep
        out += str(self.weighted)+sep
        out += str(self.favorednum)+sep
        out += str(self.weight)+sep
        out += str(self.log)+sep
        out += str(self.lifetimerolls)+sep
        out += str(self.average)+sep
        out += self.image+sep
        out += str(self.notes)

        try:
            file.write(out)
        except:
            print("Error writing file")
        finally:
            file.close()



# In[ ]:




# In[ ]:



