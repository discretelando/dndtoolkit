#!/bin/bash

# Check if python3 is a valid command
if (command -v python3)
then
    python3 "$(dirname "$BASH_SOURCE")"/toolkit/Main.py & exit
else
    # If the user renamed python3 to python
    if (command -v python)
    then
        python "$(dirname "$BASH_SOURCE")"/toolkit/Main.py & exit
    else
        read -p "This program requires Python 3 or later to run! Press any key to exit."
    fi
fi